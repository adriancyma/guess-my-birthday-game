#import randint
from random import randint

#ask player name
player_name = input("Hi! What is your name?")


for question in range(5):

    #generage random month between 1 and 12
    number_between_1_and_12 = randint(1, 12)
    month_guess = int(number_between_1_and_12)

    #generate random year between 1924 and 2004
    number_between_1924_and_2004 = randint(1924, 2004)
    year_guess = int(number_between_1924_and_2004)

    #allow 5 guess -> loop 5 times

    # print our guess month and year
    print(player_name, "were you born in",month_guess,"/",year_guess,"?")

    #ask player if guess is correct
    player_answer = input("yes or no?")

    #if wrong display message
    if player_answer == "yes":
        print ("I knew it!")
        break
    else:
        print("Drat, Lemme try again!")

#statement if fail 5 times
print("I have other things to do. Goodbye.")

